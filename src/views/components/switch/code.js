/*
 * @Description: 
 * @Date: 2021-09-29 13:46:08
 * @LastEditTime: 2021-09-29 14:00:03
 * @FilePath: \mds\src\views\mds\components\switch\code.js
 * @Author: Devin
 */
export default {
  code: `<pre class="language-markup" contenteditable="false" data-mce-selected="1"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcSwitch</span> <span class="token attr-name">v-model</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>data<span class="token punctuation">"</span></span> <span class="token punctuation">/&gt;</span></span></pre>`,
  code1:`<pre class=" language-markup" contenteditable="false" data-mce-selected="1"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcSwitch</span>
  <span class="token attr-name">v-model</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>data<span class="token punctuation">"</span></span>
  <span class="token attr-name">openMessageBox</span>
  <span class="token attr-name">url</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>/change/switch<span class="token punctuation">"</span></span>
  <span class="token attr-name">:params</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>{ name: 0 }<span class="token punctuation">"</span></span>
  <span class="token attr-name">:confirmFun</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>confirmFun<span class="token punctuation">"</span></span>
<span class="token punctuation">/&gt;</span></span></pre>`,
  code2:`<pre class="language-markup" contenteditable="false"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcSwitch</span> <span class="token attr-name">v-model</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>data<span class="token punctuation">"</span></span> <span class="token attr-name">openMessageBox</span> <span class="token attr-name">:disabled</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>true<span class="token punctuation">"</span></span><span class="token punctuation">/&gt;</span></span></pre>`
};