/*
 * @Description:
 * @Date: 2021-09-17 15:48:56
 * @LastEditTime: 2021-09-18 17:44:48
 * @FilePath: \mds\src\views\mds\components\virtual_rolling_select\code.js
 * @Author: Devin
 */
export default {
  code_1: `<pre class=" language-markup" contenteditable="false" data-mce-selected="1"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcVirtualRollingSelect</span>
  <span class="token attr-name">v-model</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>data<span class="token punctuation">"</span></span>
  <span class="token attr-name">url</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>/select/list<span class="token punctuation">"</span></span>
  <span class="token attr-name">placeholder</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>50条数据<span class="token punctuation">"</span></span>
  <span class="token attr-name">:optionKeys</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>{ value: <span class="token punctuation">'</span>id<span class="token punctuation">'</span>, label: <span class="token punctuation">'</span>name<span class="token punctuation">'</span> }<span class="token punctuation">"</span></span>
  <span class="token attr-name">filterable</span>
  <span class="token attr-name">:params</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>{ num: 50 }<span class="token punctuation">"</span></span>
<span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>CcVirtualRollingSelect</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcVirtualRollingSelect</span>
  <span class="token attr-name">v-model</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>data<span class="token punctuation">"</span></span>
  <span class="token attr-name">url</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>/select/list<span class="token punctuation">"</span></span>
  <span class="token attr-name">placeholder</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>500条数据<span class="token punctuation">"</span></span>
  <span class="token attr-name">:params</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>{ num: 500 }<span class="token punctuation">"</span></span>
  <span class="token attr-name">:optionKeys</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>{ value: <span class="token punctuation">'</span>id<span class="token punctuation">'</span>, label: <span class="token punctuation">'</span>name<span class="token punctuation">'</span> }<span class="token punctuation">"</span></span>
  <span class="token attr-name">filterable</span>
<span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>CcVirtualRollingSelect</span><span class="token punctuation">&gt;</span></span></pre>`
};
