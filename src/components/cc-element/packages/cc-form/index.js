import CcForm from './src/main'

CcForm.install = function(Vue) {
  Vue.component(CcForm.name, CcForm)
};

export default CcForm