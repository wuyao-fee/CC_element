
import CcDatePicker from './src/main'

CcDatePicker.install = function(Vue) {
  Vue.component(CcDatePicker.name, CcDatePicker)
};

export default CcDatePicker
