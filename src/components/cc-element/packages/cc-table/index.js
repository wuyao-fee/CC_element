
import CcTable from './src/main'

CcTable.install = function(Vue) {
  Vue.component(CcTable.name, CcTable)
};

export default CcTable
