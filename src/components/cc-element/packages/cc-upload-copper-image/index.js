import CcUploadCopperImage from './src/index'

CcUploadCopperImage.install = function(Vue) {
  Vue.component(CcUploadCopperImage.name, CcUploadCopperImage)
};

export default CcUploadCopperImage