
import CcDatePickerRange from './src/main'

CcDatePickerRange.install = function(Vue) {
  Vue.component(CcDatePickerRange.name, CcDatePickerRange)
};

export default CcDatePickerRange
